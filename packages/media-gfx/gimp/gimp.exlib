# Copyright 2008 Richard Brown
# Copyright 2010 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2011,2012 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

export_exlib_phases src_prepare src_configure src_install pkg_postinst pkg_postrm

SUMMARY="GNU Image Manipulation Program"
HOMEPAGE="https://www.gimp.org"
DOWNLOADS="https://download.gimp.org/pub/gimp/v$(ever range 1-2)/${PNV}.tar.bz2"

LICENCES="GPL-3 LGPL-3"
SLOT="0"
MYOPTIONS="
    aalib alsa dbus exif gtk-doc jpeg2000 lcms mng python tiff
    (
        curl    [[ description = [ Use libcurl to access remote files using the URI plugin ] ]]
        gvfs    [[ description = [ Use gvfs to access remote files using the URI plugin ] ]]
    ) [[ number-selected = at-most-one ]]
    pdf         [[ description = [ Support importing PDF (Portable Document Format) files ] ]]
    postscript  [[ description = [ Support importing PS (PostScript) files ] ]]
    svg         [[ description = [ Support importing SVG (Scalable Vector Graphics) files ] ]]
    wmf         [[ description = [ Support for the Windows Metafile image format ] ]]

    platform: amd64
    x86_cpu_features: mmx sse
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# tests attempt to use X and fail
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.1]
        sys-devel/gettext[>=0.19]
        virtual/pkg-config[>=0.16]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        app-text/iso-codes
        app-text/poppler[>=0.12.4][cairo]
        dev-libs/atk[>=2.2.0]
        dev-libs/glib:2[>=2.30.2]
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        gnome-desktop/libgudev[>=167]
        media-libs/babl[>=0.1.10]
        media-libs/fontconfig[>=2.2.0]
        media-libs/freetype:2
        media-libs/gegl:0[>=0.2.0]
        media-libs/libpng:=[>=1.2.37]
        sys-libs/zlib
        x11-libs/cairo[>=1.10.2]      [[ note = [ needs cairo-pdf, which we have enabled by default ] ]]
        x11-libs/gdk-pixbuf:2.0[>=2.24.1]
        x11-libs/gtk+:2[>=2.24.10]
        x11-libs/libX11
        x11-libs/libXcursor           [[ note = [ hard-enabled because gtk+ needs it anyway ] ]]
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXmu
        x11-libs/libXpm               [[ note = [ hard-enabled because people who have xorg-server or xterm have it installed anyway and it's small ] ]]
        x11-libs/pango[>=1.29.4]
        aalib?      ( media-libs/aalib                   )
        alsa?       ( sys-sound/alsa-lib[>=1.0.0]        )
        curl?       ( net-misc/curl[>=7.15.1]            )
        dbus?       ( dev-libs/dbus-glib:1[>=0.70]       )
        exif?       ( media-libs/libexif[>=0.6.15]       )
        gvfs?       ( gnome-desktop/gvfs                 )
        jpeg2000?   ( media-libs/jasper                  )
        lcms?       ( media-libs/lcms2[>=2.2]            )
        mng?        ( media-libs/libmng                  )
        pdf?        ( app-text/poppler[>=0.12.4][glib]   )
        postscript? ( app-text/ghostscript               )
        providers:ijg-jpeg? ( media-libs/jpeg:=          )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        python?     ( dev-lang/python:*[>=2.5]
                      gnome-bindings/pygtk:2[>=2.10.4]   ) [[ note = [ needs to be tested with python3 ] ]]
        svg?        ( gnome-desktop/librsvg:2[>=2.36.0]  )
        tiff?       ( media-libs/tiff                    )
        wmf?        ( media-libs/libwmf[>=0.2.8]         )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=167] )
    run:
        x11-themes/hicolor-icon-theme
"

gimp_src_prepare() {
    edo sed -e "/AC_PATH_PROG(PKG_CONFIG/d" -i m4macros/gimp-2.0.m4

    edo sed -i -e "s:pkg-config:$(exhost --tool-prefix)&:" tools/gimptool.c

    # Respect datarootdir
    # can't use intoolize because gimg has a strange po handling
    edo sed -e 's:itlocaledir = $(prefix)/$(DATADIRNAME)/locale:itlocaledir = $(datarootdir)/locale:' \
        -i "${WORK}"/po*/Makefile.in.in

    autotools_src_prepare
}

gimp_src_configure() {
    local myconf=()

    myconf+=(
        # This could maybe be used to enable tests, if it was packaged
        --without-xvfb-run

        # always enable
        --enable-default-binary
        --enable-mp
        --enable-nls
        --with-libjpeg
        --with-libpng
        --with-print
        --with-xmc

        # always disable
        --without-webkit
    )

    # If I don't do this, they are forced off
    if option platform:amd64 ; then
        myconf+=(
            --enable-mmx
            --enable-sse
        )
    else
        myconf+=(
            $(option_enable x86_cpu_features:mmx)
            $(option_enable x86_cpu_features:sse)
        )
    fi

    myconf+=(
        $(option lcms --with-lcms=2 --without-lcms)
        $(option_enable gtk-doc)
        $(option_enable python)
        $(option_with aalib aa)
        $(option_with alsa)
        $(option_with curl libcurl)
        $(option_with dbus)
        $(option_with exif libexif)
        $(option_with gvfs)
        $(option_with jpeg2000 libjasper)
        $(option_with mng libmng)
        $(option_with pdf poppler)
        $(option_with postscript gs)
        $(option_with svg librsvg)
        $(option_with tiff libtiff)
        $(option_with wmf)
    )

    econf "${myconf[@]}"
}

gimp_src_install() {
    default
    keepdir /usr/share/gimp/2.0/fonts
}

gimp_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

gimp_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

