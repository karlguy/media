# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2013 Lasse Brun <bruners@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="${PN}${PV:0:1}-${PV}"

require cmake [ api=2 cmake_minimum_version=2.8.11 ]

SUMMARY="The Simple DirectMedia Layer library"
HOMEPAGE="https://libsdl.org"
DOWNLOADS="${HOMEPAGE}/release/${MY_PNV}.tar.gz"

LICENCES="ZLIB"
SLOT="2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    X
    jack
    libsamplerate
    pulseaudio
    wayland
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# Building the tests results in an install error, last checked: 2.0.8
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        wayland? ( sys-libs/wayland-protocols )
    build+run:
        sys-apps/dbus
        sys-sound/alsa-lib
        x11-dri/libdrm
        x11-dri/mesa
        jack? ( media-sound/jack-audio-connection-kit )
        libsamplerate? ( media-libs/libsamplerate )
        pulseaudio? ( media-sound/pulseaudio )
        wayland? (
            sys-libs/wayland
            x11-dri/mesa[wayland]
            x11-libs/libxkbcommon
        )
        X? (
            x11-libs/libX11
            x11-libs/libXcursor
            x11-libs/libXext
            x11-libs/libXi
            x11-libs/libXinerama
            x11-libs/libXt
            x11-libs/libXrender
            x11-libs/libXrandr
            x11-libs/libXScrnSaver
            x11-libs/libICE
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

CMAKE_SOURCE=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( WhatsNew )

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DALSA:BOOL=TRUE
    -DARTS:BOOL=FALSE
    -DARTS_SHARED_BOOL=FALSE
    -DESD:BOOL=FALSE
    -DFUSIONSOUND:BOOL=FALSE
    -DFUSIONSOUND_SHARED:BOOL=FALSE
    -DINPUT_TSLIB:BOOL=FALSE
    -DKMSDRM_SHARED:BOOL=TRUE
    -DNAS:BOOL=FALSE
    -DOSS:BOOL=TRUE
    -DRPATH:BOOL=FALSE
    -DVIDEO_KMSDRM:BOOL=TRUE
    -DVIDEO_MIR:BOOL=FALSE
    -DVIDEO_OPENGL:BOOL=TRUE
    -DVIDEO_OPENGLES:BOOL=TRUE
    -DVIDEO_VULKAN:BOOL=TRUE
    -DVIDEO_WAYLAND_QT_TOUCH:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'X X11_SHARED'
    'X VIDEO_X11'
    'X VIDEO_X11_XCURSOR'
    'X VIDEO_X11_XINERAMA'
    'X VIDEO_X11_XINPUT'
    'X VIDEO_X11_XRANDR'
    'X VIDEO_X11_XSCRNSAVER'
    'X VIDEO_X11_XSHAPE'
    'X VIDEO_X11_XVM'
    'jack JACK'
    'jack JACK_SHARED'
    'libsamplerate LIBSAMPLERATE'
    'libsamplerate LIBSAMPLERATE_SHARED'
    'pulseaudio PULSEAUDIO'
    'pulseaudio PULSEAUDIO_SHARED'
    'wayland VIDEO_WAYLAND'
    'wayland WAYLAND_SHARED'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DSDL_TEST:BOOL=TRUE -DSDL_TEST:BOOL=FALSE'
)

