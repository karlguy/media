# Copyright 2009 Richard Brown <rbrown@exherbo.org>
# Copyright 2011, 2012 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require alternatives

if ever is_scm; then
    require gnome.org
else
    DOWNLOADS="https://download.gimp.org/pub/${PN}/$(ever range 1-2)/${PNV}.tar.bz2"
fi

export_exlib_phases src_prepare src_configure src_install

SUMMARY="GEGL (Generic Graphics Library) is a graph based image processing framework"
DESCRIPTION="
GEGL provides infrastructure to do demand based cached non destructive image editing
on larger than RAM buffers. Through babl it provides support for a wide range of color
models and pixel storage formats for input and output.
"
HOMEPAGE="http://${PN}.org"

UPSTREAM_RELEASE_NOTES="https://git.gnome.org/browse/${PN}/plain/NEWS"

LICENCES="GPL-3 LGPL-3"
MYOPTIONS="
    exif ffmpeg jpeg2000 openexr raw sdl svg
    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
    ( x86_cpu_features: mmx sse )
    ( platform: amd64 )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# TODO: --enable-docs has automagic deps upon asciidoc, enscript & ruby
# Also, they always rebuild and only install if the tools above are present.
# See:  https://bugzilla.gnome.org/show_bug.cgi?id=676688
DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.36.0]
        media-libs/babl[$(ever is_scm && echo '~scm' || echo '>=0.1.12')]
        media-libs/libpng:=
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[>=2.18.0]
        x11-libs/pango
        exif? ( graphics/exiv2 )
        ffmpeg? (
            providers:ffmpeg? ( media/ffmpeg[>=0.11.1] )
            providers:libav? ( media/libav[>=9.1] )
        )
        jpeg2000? ( media-libs/jasper[>=1.900.1] )
        openexr? ( media-libs/openexr )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        sdl? ( media-libs/SDL:0 )
        svg? ( gnome-desktop/librsvg:2[>=2.14.0] )
"
# needs babl[gobject-introspection?], which is broken
# See https://bugzilla.gnome.org/show_bug.cgi?id=673422
#    build:
#        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.32.0] )
# tests fail softly with introspection
#    test:
#        gobject-introspection? ( dev-lang/python:*[>=2.5]
#                                 gnome-bindings/pygobject:3[>=3.2] )

if ever at_least 0.3.30; then
    MYOPTIONS+="
        lcms
        tiff
        v4l [[ description = [ V4L2 device support ] ]]
        webp [[ description = [ Support for the WebP image format ] ]]
        ( linguas: bs ca da de el en_GB eo es eu fr gl id is it ko lv nb oc pl pt_BR pt ru sk sl sr
                   sv tr zh_CN )
    "

    DEPENDENCIES+="
        build:
            sys-devel/gettext[>=0.19.8]
        build+run:
            core/json-glib
            dev-libs/glib:2[>=2.44.0]
            media-libs/babl[$(ever is_scm && echo '~scm' || echo '>=0.1.44')]
            media-libs/libpng:=[>=1.6.0]
            x11-libs/cairo[>=1.12.2]
            x11-libs/pango[>=1.38.0]
            x11-libs/gdk-pixbuf:2.0[>=2.32.0]
            exif? ( graphics/exiv2[>=0.25] )
            ffmpeg? (
                providers:ffmpeg? ( media/ffmpeg[>=2.3] )
                providers:libav? ( media/libav[>=9.1] )
            )
            lcms? ( media-libs/lcms2[>=2.8] )
            openexr? ( media-libs/openexr[>=1.6.1] )
            raw? ( media-libs/libraw[>=0.15.4] )
            sdl? ( media-libs/SDL:0[>=1.2.0] )
            svg? ( gnome-desktop/librsvg:2[>=2.40.6] )
            tiff? ( media-libs/tiff[>=4.0.0] )
            v4l? ( media-libs/v4l-utils[>=1.0.1] )
            webp? ( media-libs/libwebp:=[>=0.5.0] )
            !media-libs/gegl:0[<=0.2.0-r7] [[
                description = [ /usr/bin/gegl collides ]
                resolution = upgrade-blocked-before
            ]]
    "
else
    DEPENDENCIES+="
        build:
            dev-util/intltool[>=0.40.1]
        build+run:
            ffmpeg? (
                providers:ffmpeg? ( media/ffmpeg[>=0.11.1] )
                providers:libav? ( media/libav[>=9.1] )
            )
            raw? ( media-libs/libopenraw[>=0.0.5] )
    "
fi

AT_M4DIR=( m4 )

gegl_src_prepare() {
    # Exclude a failing test from being run
    # See https://bugzilla.gnome.org/show_bug.cgi?id=642390
    ever at_least 0.3 || edo sed -i -e 's#test-exp-combine.sh##g' tests/simple/Makefile.am

    # Trade segfaults in for leaks. img_cmp is not installed.
    # No longer present in scm, but non-trivial to backport.
    ever at_least 0.3 || edo sed -i -e '/g_object_unref (buffer/d' tools/img_cmp.c

    # Tests failing due to babl changes
    # Binary patch to reference files is 3MB so just diable them
    # https://git.gnome.org/browse/gegl/commit/tests?h=gegl-0-2&id=0f5227d9502bbd2ba4eb1c9d118d1b82138a7a5c
    ever at_least 0.3 || edo sed -i -e '/run-clones.xml.sh/d'    \
        -e '/run-fattal02.xml.sh/d' -e '/run-mantiuk06.xml.sh/d' \
        tests/compositions/Makefile.am

    # Disable failing test, last checked: 0.3.0
    ever at_least 0.3.0 && edo sed -e 's#test-image-compare##g' -i tests/simple/Makefile.am

    # Disable failing test, last checked: 0.3.20
    # /bin/sh: line 1: ../../examples/gegl-video: No such file or directory
    ever at_least 0.3.8 && edo sed -e '/ff-load-save/d' -i tests/Makefile.am

    # don't install locales to /usr/${target}/share
    # drop if po/Makefile.in.in doesn't have 'itlocaledir = $(prefix)/$(DATADIRNAME)/locale'
    ever at_least 0.3.26 || edo intltoolize --force --copy --automake

    autotools_src_prepare
}

gegl_src_configure() {
    local myconf=()

    myconf+=(
        --prefix=/usr
        --exec_prefix=/usr/$(exhost --target)
        --includedir=/usr/$(exhost --target)/include
        # 'gcut' tool name conflicts with cut from coreutils
        # https://bugzilla.gnome.org/show_bug.cgi?id=786838
        --program-transform-name='s/^gcut$/gegl-gcut/'
        --disable-docs
        --disable-introspection
        --with-cairo
        --with-gdk-pixbuf
        --with-pango
        --with-pangocairo
        --without-graphviz
        --without-lensfun
        --without-libspiro
        --without-lua
        --without-umfpack
        --without-vala # currently broken, upstream has a fix
        $(option_with exif exiv2)
        $(option_with ffmpeg libavformat)
        $(option_with jpeg2000 jasper)
        $(option_with openexr)
        $(option_with sdl)
        $(option_with svg librsvg)
    )

    # If I don't do this, they are forced off
    if option platform:amd64 ; then
        myconf+=(
            --enable-mmx
            --enable-sse
        )
    else
        myconf+=(
            $(option_enable x86_cpu_features:mmx)
            $(option_enable x86_cpu_features:sse)
        )
    fi

    if ever at_least 0.3.14; then
        myconf+=(
            # mrg and gexiv2 needed for gui, mrg unpackaged
            --without-gexiv2
            --without-mrg
            $(option_with lcms)
            $(option_with raw libraw)
            $(option_with tiff libtiff)
            $(option_with v4l libv4l)
            $(option_with v4l libv4l2)
            $(option_with webp)
         )
    else
        myconf+=(
            --with-libjpeg
            --with-libpng
            --without-libv4l
            $(option_with raw libopenraw)
        )
    fi

    econf "${myconf[@]}"
}

gegl_src_install() {
    default

    local n
    if [[ ${SLOT} = 0 ]] ; then
        n=0.2
    else
        n=${SLOT}
    fi

    alternatives_for _${PN} ${PN}-${n} ${n} /usr/$(exhost --target)/bin/${PN} ${PN}-${n}
}

