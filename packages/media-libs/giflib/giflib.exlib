# Copyright 2013 Ankur Kothari
# Copyright 2008 Kim Højgaard-Hansen
# Copyright 2014 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge alternatives

export_exlib_phases src_configure src_install pkg_postinst

SUMMARY="Library for processing GIFs"
DESCRIPTION="
giflib provides code for reading GIF files and transforming them into RGB bitmaps, and for writing
RGB bitmaps as GIF files.
"

LICENCES="MIT"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        !media-libs/giflib:0 [[
            description = [ Uninstall media-libs/giflib:0 after switching to the slotted version ]
            resolution = uninstall-blocked-after
        ]]
"

if ever at_least 5.1.0; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=( --disable-static )
else
    DEPENDENCIES+="
        build:
            app-text/docbook-xml-dtd:4.1.2
    "
fi

giflib_src_configure() {
    ever at_least 5.1.0 && export ac_cv_prog_have_xmlto=no

    default
}

giflib_src_install() {
    default

    local host=$(exhost --target)

    local libraries=(
        libgif.la libgif${SLOT}.la
        libgif.so libgif${SLOT}.so
    )

    for (( i = 0; i < ${#libraries[@]}; i++ )); do
        edo mv "${IMAGE}"/usr/${host}/lib/${libraries[i]} "${IMAGE}"/usr/${host}/lib/${libraries[i+1]}
        alternatives_for _${PN} ${SLOT} ${SLOT} /usr/${host}/lib/${libraries[i]} ${libraries[++i]}
    done

    local headers=(
        gif_lib.h gif_lib${SLOT}.h
    )

    # Do the actual renaming and creating light alternatives
    edo pushd "${IMAGE}"/usr/${host}/bin
    for binary in * ; do
        edo mv "${IMAGE}"/usr/${host}/bin/${binary}{,${SLOT}}
        alternatives_for _${PN} ${SLOT} ${SLOT} /usr/${host}/bin/${binary} ${binary}${SLOT}
    done
    edo popd

    for (( i = 0; i < ${#headers[@]}; i++ )); do
        edo mv "${IMAGE}"/usr/${host}/include/${headers[i]} "${IMAGE}"/usr/${host}/include/${headers[i+1]}
        alternatives_for _${PN} ${SLOT} ${SLOT} /usr/${host}/include/${headers[i]} /usr/${host}/include/${headers[++i]}
    done
}

giflib_pkg_postinst() {
    # We can't force alternatives to overwrite files with symlinks
    if has_version --root "media-libs/giflib:0"; then
        local files=(
            "${ROOT}"/usr/${host}/bin/gif{2epsn,2ps,2rgb,2x11,asm,bg,build,burst,clip,clrmp,color,comb,compose,echo,filtr}
            "${ROOT}"/usr/${host}/bin/gif{fix,flip,histo,info,inter,into,ovly,pos,rotat,rsize,spnge,text,tool,wedge}
            "${ROOT}"/usr/${host}/bin/{icon2gif,raw2gif,rgb2gif,text2gif}
            "${ROOT}"/usr/${host}/include/gif_lib.h
            "${ROOT}"/usr/${host}/lib/libgif.la
        )
        for f in "${files[@]}" ; do
            [[ -e ${f} && ! -L ${f} ]] && ( nonfatal edo rm "${f}" || eerror "rm -r ${f}" );
        done
    fi

    alternatives_pkg_postinst
}

