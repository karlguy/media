# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="A library for interfacing MPD in the C, C++ & Objective C languages"
DESCRIPTION="
A stable, documented, asynchronous API library for interfacing MPD in the C, C++
& Objective C languages.
"
HOMEPAGE="https://www.musicpd.org/libs/${PN}"

BUGS_TO="alip@exherbo.org"

UPSTREAM_RELEASE_NOTES="http://git.musicpd.org/cgit/master/${PN}.git/plain/NEWS?h=v${PV}"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="doc"

# https://github.com/MusicPlayerDaemon/libmpdclient/issues/8
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen[dot] )
    build+test:
        dev-libs/check
"

src_configure() {
    local myconf=(
        -Ddefault_socket=/run/mpd/socket
        -Ddocumentation=$(option doc true false)
        -Dtcp=true
        -Dtest=$(expecting_tests && echo true || echo false)
    )

    exmeson "${myconf[@]}" ${MESON_SOURCE}
}

