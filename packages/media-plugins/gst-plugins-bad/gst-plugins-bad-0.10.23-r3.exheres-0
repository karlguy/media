# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2009, 2011 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gsettings

SUMMARY="A set of plugins that need more quality"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.bz2"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
    gstreamer_plugins:
        apex         [[ description = [ AirPort Express Wireless Support ] ]]
        ass          [[ description = [ ASS/SSA (Advanced Substation Alpha/Substation Alpha) subtitle rendering using libass ] ]]
        celt         [[ description = [ Support for the CELT low-delay audio codec ] ]]
        cog          [[ description = [ Orc based GStreamer plugins ] ]]
        curl         [[ description = [ Support uploading data to a server using curl ] ]]
        dc1394       [[ description = [ Support for getting data from IIDC FireWire cameras using libdc1394 ] ]]
        dirac        [[ description = [ Dirac video stream encoding using the dirac-research library (it's recommended to use the Schroedinger implementation) ] ]]
        dts          [[ description = [ DTS (multichannel digital surround sound) audio decoding using libdca ] ]]
        faac         [[ description = [ AAC audio encoding using faac ] ]]
        faad         [[ description = [ MPEG-2/4 AAC audio decoding using faad2 ] ]]
        gsm          [[ description = [ GSM audio decoding and encoding ] ]]
        jpeg2000     [[ description = [ JPEG 2000 image compression and decompression using libjasper ] ]]
        kate         [[ description = [ Kate (karaoke and text codec) stream encoding/decoding using libkate and rendering using libtiger ] ]]
        ladspa       [[ description = [ Linux Audio Developer Simple Plugin API support ] ]]
        libmms       [[ description = [ Support for mms:// and mmsh:// network streams ] ]]
        lv2          [[ description = [ Support for LV2 (simple but extensible successor of LADSPA) plugins ] ]]
        mjpeg        [[ description = [ MPEG-1/2 video encoding and MPEG/DVD/SVCD/VCD video/audio multiplexing using mjpegtools ] ]]
        musepack     [[ description = [ Musepack audio decoding using libmpcdec ] ]]
        neon         [[ description = [ HTTP source handling using neon ] ]]
        ofa          [[ description = [ MusicIP audio fingerprint calculation using libofa ] ]]
        openal       [[ description = [ OpenAL sink ] ]]
        opencv       [[ description = [ Computer Vision techniques from OpenCV ] ]]
        opus         [[ description = [ Opus audio decoding and encoding ] ]]
        resin        [[ description = [ DVD playback plugin ] ]]
        schroedinger [[ description = [ Dirac video encoding using schroedinger ] ]]
        sdl          [[ description = [ SDL-based audio and video output ] ]]
        soundtouch   [[ description = [ BPM detection and audio pitch controlling using soundtouch ] ]]
        sndfile      [[ description = [ Support for reading and writing audio data using libsndfile ] ]]
        svg          [[ description = [ SVG image decoding using librsvg ] ]]
        swfdec       [[ description = [ Flash video decoding using swfdec ] ]]
        vdpau        [[ description = [ Offload parts of video encoding to the GPU using VDPAU ] ]]
        vo-aacenc    [[ description = [ AAC encoder using vo-aacenc ] ]]
        vo-amrwbenc  [[ description = [ OpenCORE AMR-WB decoder and encoder (audio) ] ]]
        vp8          [[ description = [ VP8 support using libvpx ] ]]
        rtmp         [[ description = [ Support for rtmp:// and rtmpe:// network streams ] ]]
        xvid         [[ description = [ Xvid encoding and decoding using the xvidcore library ] ]]

    gstreamer_plugins:apex? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
"

# cogorc_resample_horiz_1tap/2tap - compiled function: compile failed
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-util/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.3] )
    test:
        media-libs/libexif[>=0.6.16]
    build+run:
        media-libs/gstreamer:0.10[>=0.10.36]
        media-plugins/gst-plugins-base:0.10[>=0.10.36]
        dev-libs/glib:2[>=2.25]
        dev-libs/orc:0.4[>=0.4.11]
        gstreamer_plugins:openal? ( media-libs/openal )
        gstreamer_plugins:apex? ( 
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        )
        gstreamer_plugins:ass? ( media-libs/libass[>=0.9.4] )
        gstreamer_plugins:celt? ( media-libs/celt[>=0.8] )
        gstreamer_plugins:cog? ( media-libs/libpng[>=1.2] )
        gstreamer_plugins:curl? ( net-misc/curl[>=7.21.0] )
        gstreamer_plugins:dc1394? ( media-libs/libdc1394:2[>=2.0.0] )
        gstreamer_plugins:dirac? ( media-libs/dirac[>=0.10] )
        gstreamer_plugins:dts? ( media-libs/libdca )
        gstreamer_plugins:faac? ( media-libs/faac )
        gstreamer_plugins:faad? ( media-libs/faad2[>=2.0] )
        gstreamer_plugins:gsm? ( media-libs/gsm )
        gstreamer_plugins:jpeg2000? ( media-libs/jasper )
        gstreamer_plugins:kate? ( media-libs/libkate[>=0.1.7]
                                  media-libs/libtiger[>=0.3.2] )
        gstreamer_plugins:ladspa? ( media-libs/ladspa-sdk
                                    media-libs/liblrdf )
        gstreamer_plugins:libmms? ( media-libs/libmms )
        gstreamer_plugins:lv2? ( media-libs/slv2[>=0.6.6] )
        gstreamer_plugins:musepack? ( media-libs/libmpcdec )
        gstreamer_plugins:mjpeg? ( media-video/mjpegtools[>=2.0.0] )
        gstreamer_plugins:neon? ( net-misc/neon[>=0.27.0&<=0.30.99] )
        gstreamer_plugins:ofa? ( media-libs/libofa[>=0.9.3] )
        gstreamer_plugins:opencv? ( media-libs/opencv[>=2.0.0&<=2.3.1] )
        gstreamer_plugins:opus? ( media-libs/opus[>=0.9.4] )
        gstreamer_plugins:resin? ( media-libs/libdvdnav[>=4.1.2]
                                   media-libs/libdvdread[>=4.1.2] )
        gstreamer_plugins:schroedinger? ( media-libs/schroedinger[>=1.0.10] )
        gstreamer_plugins:sdl? ( media-libs/SDL )
        gstreamer_plugins:soundtouch? ( media-libs/soundtouch )
        gstreamer_plugins:sndfile? ( media-libs/libsndfile[>=1.0.16] )
        gstreamer_plugins:svg? ( gnome-desktop/librsvg:2[>=2.14] )
        gstreamer_plugins:swfdec? ( media-libs/swfdec:0.3[>=0.3.6] )
        gstreamer_plugins:vdpau? ( x11-libs/libvdpau )
        gstreamer_plugins:vo-aacenc? ( media-libs/vo-aacenc[>=0.1.0] )
        gstreamer_plugins:vo-amrwbenc? ( media-libs/vo-amrwbenc[>=0.1.0] )
        gstreamer_plugins:vp8? ( media-libs/libvpx )
        gstreamer_plugins:rtmp? ( media-video/rtmpdump )
        gstreamer_plugins:xvid? ( media-libs/xvid )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.10.23-CVE-2015-0797.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-experimental'
    '--disable-examples'

    # core plugins
    '--enable-adpcmdec'
    '--enable-adpcmenc'
    '--enable-aiff'
    '--enable-asfmux'
    '--enable-audiovisualizers'
    '--enable-autoconvert'
    '--enable-bayer'
    '--enable-camerabin'
    '--enable-camerabin2'
    '--enable-cdxaparse'
    '--enable-coloreffects'
    '--enable-colorspace'
    '--enable-dataurisrc'
    '--enable-dccp'
    '--enable-debugutils'
    '--enable-dtmf'
    '--enable-dvbsuboverlay'
    '--enable-dvdspu'
    '--enable-faceoverlay'
    '--enable-festival'
    '--enable-fieldanalysis'
    '--enable-freeze'
    '--enable-freeverb'
    '--enable-frei0r'
    '--enable-gaudieffects'
    '--enable-geometrictransform'
    '--enable-gsettings'
    '--enable-h264parse'
    '--enable-hdvparse'
    '--enable-hls'
    '--enable-id3tag'
    '--enable-inter'
    '--enable-interlace'
    '--enable-ivfparse'
    '--enable-jp2kdecimator'
    '--enable-jpegformat'
    '--enable-legacyresample'
    '--enable-librfb'
    '--enable-liveadder'
    '--enable-mpegdemux'
    '--enable-mpegtsdemux'
    '--enable-mpegtsmux'
    '--enable-mpegpsmux'
    '--enable-mpegvideoparse'
    '--enable-mve'
    '--enable-mxf'
    '--enable-nuvdemux'
    '--enable-patchdetect'
    '--enable-orc'
    '--enable-pcapparse'
    '--enable-pnm'
    '--enable-rawparse'
    '--enable-real'
    '--enable-removesilence'
    '--enable-rtpmux'
    '--enable-rtpvp8'
    '--enable-scaletempo'
    '--enable-sdi'
    '--enable-sdp'
    '--enable-segmentclip'
    '--enable-shm'
    '--enable-siren'
    '--enable-smooth'
    '--enable-speed'
    '--enable-subenc'
    '--enable-stereo'
    '--enable-tta'
    '--enable-videofilters'
    '--enable-videomaxrate'
    '--enable-videomeasure'
    '--enable-videoparsers'
    '--enable-videosignal'
    '--enable-y4m'

    # Windows Specific
    '--disable-direct3d'
    '--disable-directdraw'
    '--disable-directsound'
    '--disable-wininet'
    '--disable-acm'

    # OS X Specific
    '--disable-apple-media'
    '--disable-avc'

    # Unpackaged dependencies
    '--disable-cdaudio'
    '--disable-chromaprint'
    '--disable-directfb'
    '--disable-divx'
    '--disable-flite'
    '--disable-gme'
    '--disable-mimic'
    '--disable-modplug'
    '--disable-mythtv'
    '--disable-nas'
    '--disable-pvr'
    '--disable-quicktime'
    '--disable-spandsp'
    '--disable-spc'
    '--disable-teletextdec'
    '--disable-timidity'
    '--disable-wildmidi'
    '--disable-zbar'

    '--enable-bz2'
    '--enable-decklink'
    '--enable-dvb'
    '--enable-fbdev'
    '--enable-linsys'
    '--enable-vcd'

    # only works with libmusicbrainz:2 which is deprecated and broken
    '--disable-musicbrainz'

    # Disabled for security reasons: CVE-2016-944{5,6,7}
    '--disable-nsf'
    '--disable-vmnc'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gtk-doc'

    'gstreamer_plugins:apex apexsink'
    'gstreamer_plugins:ass assrender'
    'gstreamer_plugins:celt'
    'gstreamer_plugins:cog'
    'gstreamer_plugins:curl'
    'gstreamer_plugins:dc1394'
    'gstreamer_plugins:dirac'
    'gstreamer_plugins:dts'
    'gstreamer_plugins:faac'
    'gstreamer_plugins:faad'
    'gstreamer_plugins:gsm'
    'gstreamer_plugins:jpeg2000 jp2k'
    'gstreamer_plugins:kate'
    'gstreamer_plugins:ladspa'
    'gstreamer_plugins:libmms'
    'gstreamer_plugins:lv2'
    'gstreamer_plugins:musepack'
    'gstreamer_plugins:mjpeg mpeg2enc'
    'gstreamer_plugins:mjpeg mplex'
    'gstreamer_plugins:neon'
    'gstreamer_plugins:ofa'
    'gstreamer_plugins:openal'
    'gstreamer_plugins:opencv'
    'gstreamer_plugins:opus'
    'gstreamer_plugins:resin resindvd'
    'gstreamer_plugins:schroedinger schro'
    'gstreamer_plugins:sdl'
    'gstreamer_plugins:soundtouch'
    'gstreamer_plugins:sndfile'
    'gstreamer_plugins:svg rsvg'
    'gstreamer_plugins:swfdec'
    'gstreamer_plugins:vdpau'
    'gstreamer_plugins:vo-aacenc voaacenc'
    'gstreamer_plugins:vo-amrwbenc voamrwbenc'
    'gstreamer_plugins:vp8'
    'gstreamer_plugins:rtmp'
    'gstreamer_plugins:xvid'
)

src_prepare() {
    # https://bugzilla.gnome.org/show_bug.cgi?id=705812
    edo sed -e 's:0.29.99:0.30.99:g' \
            -i configure{,.ac}

    # fix building with libvpx-1.4.0
    edo sed -e '/Some compatibility/ s:*/::' \
            -e '/const char/ i*/' \
            -i  ext/vp8/gstvp8utils.h

    default
}

src_test() {
    unset DBUS_SESSION_BUS_ADDRESS
    unset DISPLAY
    default
}

