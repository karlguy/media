# Copyright 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A low-latency audio server"
DESCRIPTION="
JACK a system for sending audio between applications as well as allowing them
to all share an audio interface. Each application is run in exact sample sync
with each other, and the entire system is designed to run at very low latencies
(as low as your hardware will allow)."
HOMEPAGE="http://www.jackaudio.org"
DOWNLOADS="${HOMEPAGE}/downloads/${PNV}.tar.gz"

REMOTE_IDS="freecode:jackit"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    alsa
    debug
    firewire
    portaudio
    x86_cpu_features: sse
"

# XXX: Currently automagic:
#     media-libs/celt:0.5.1[>=0.5.1] [[ description = [ build NetJack with CELT support, for transmission via internet ] ]]
DEPENDENCIES="
    build:
        app-doc/doxygen
    build+run:
        media-libs/libsamplerate[>=0.1.2] [[ description = [ NetJack backend and internal client ] ]]
        media-libs/libsndfile[>=1.0] [[ description = [ jackrec example client ] ]]
        sys-libs/db:*
        alsa? ( sys-sound/alsa-lib[>=1.0.18] )
        firewire? ( media-libs/libffado[>=1.999.17] )
        portaudio? ( media-libs/portaudio )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-force-install
    --disable-capabilities
    # Mac OS X audio output driver
    --disable-coreaudio
    # Old Firewire support, use option=firewire
    --disable-freebob
    --disable-oss
    --disable-sndio
    --disable-sun
    --with-html-dir=/usr/share/doc/${PNVR}
    # zalsa requires zita-resampler and zita-alsa-pcmi
    --disable-zalsa
)

# XXX: Work out wtb these configure options do:
# --enable-optimize
# --enable-optimization-by-compiler
# --enable-optimization-by-cpu
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    alsa
    debug
    firewire
    portaudio
    'x86_cpu_features:sse'
)

