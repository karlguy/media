# Copyright 2012, 2013 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Utility used for retrieving technical information and metadata from audio/video files"
HOMEPAGE="https://mediaarea.net/mediainfo"
DOWNLOADS="https://mediaarea.net/download/source/${PN}/${PV}/${PN}_${PV}.tar.xz"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7"
MYOPTIONS="
    gui [[ description = [ Additionally build the wxWidgets-based graphical user interface ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libmediainfo
        media-libs/libzen
        gui? ( x11-libs/wxGTK:=[>=2.6.0] )
"

WORK=${WORKBASE}/MediaInfo

pkg_setup() {
    TARGETS="CLI"
    option gui && TARGETS+=" GUI"
}

src_prepare() {
    local target

    for target in ${TARGETS}; do
        edo pushd Project/GNU/${target}
        # TODO: report upstream
        edo sed \
            -e "s:pkg-config:$(exhost --tool-prefix)&:g" \
            -i configure.ac

        autotools_src_prepare
        edo popd
    done
}

src_configure() {
    local target
    local myconf=()

    for target in ${TARGETS}; do
        edo pushd Project/GNU/${target}
        if [[ ${target} == "GUI" ]]; then
            myconf+=(
                --with-wx-gui
                --with-wxwidgets
            )
        fi

        econf "${myconf[@]}"
        edo popd
    done
}

src_compile() {
    local target

    for target in ${TARGETS}; do
        edo pushd Project/GNU/${target}
        default
        edo popd
    done
}

src_install () {
    local target

    for target in ${TARGETS}; do
        edo pushd Project/GNU/${target}
        default
        edo popd
    done
}

pkg_postinst() {
    option gui && freedesktop-desktop_pkg_postinst
    option gui && gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    option gui && freedesktop-desktop_pkg_postrm
    option gui && gtk-icon-cache_pkg_postrm
}

